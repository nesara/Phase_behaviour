import java.io.IOException;
import java.io.DataInput;
import java.io.DataOutput;
import java.util.StringTokenizer;
import java.util.*;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import java.lang.Object;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import java.math.BigInteger;
import java.math.BigDecimal;
import java.math.*;


public class CosineSimilarity{
public static class CosineSimilarityMapper extends Mapper
<Object,Text,Text,IntWritable>{
	private IntWritable result = new IntWritable();
    public void map(Object key, Text value, Context context
                    ) throws IOException, InterruptedException,ArithmeticException {
            StringTokenizer itr = new StringTokenizer(value.toString()," ");
            
            int[] intArray = new int[8];
            int i=0;
        while (itr.hasMoreTokens()) {
           String data1=itr.nextToken();
           int data2=Integer.parseInt(data1);
           intArray[i++]=data2;
      }
           BigInteger dval1=BigInteger.ZERO;
           BigInteger dval2=BigInteger.ZERO;
           BigInteger bval1,bval2,cval1,cval2,resd1,resd2,y1,y2,tres;
          // d1=BigInteger.valueOf(Math.pow(intArray[0],2));
           //d2=BigInteger.valueOf(Math.pow(intArray[4],2));
           //int d1=0;
           //int d2=0;
           int aval1,aval2;
           for(i=0;i<4;i++){
           //d1=d1.add(BigInteger.valueOf(Math.pow(intArray[i],2)));
           aval1=intArray[i];
           bval1=BigInteger.valueOf(aval1);
           cval1=bval1.multiply(bval1);
           dval1=dval1.add(cval1);
           }
           for(i=4;i<8;i++){
           //d2=d2.add(BigInteger.valueOf(Math.pow(intArray[i],2)));
           aval2=intArray[i];
           bval2=BigInteger.valueOf(aval2);
           cval2=bval2.multiply(bval2);
           dval2=dval2.add(cval2);

           }
           
           BigInteger div1=BigInteger.ZERO.setBit(dval1.bitLength()/2);
           BigInteger div11=div1;
           for(;;){
               y1=div1.add(dval1.divide(div1)).shiftRight(1);
               if(y1.equals(div1)||y1.equals(div11)){
                   resd1=y1;
                   break;}
               div11=div1;
               div1=y1;
            }
           BigInteger div2=BigInteger.ZERO.setBit(dval2.bitLength()/2);
           BigInteger div22=div2;
           for(;;){
               y2=div2.add(dval2.divide(div2)).shiftRight(1);
               if(y2.equals(div2)||y2.equals(div22)){
                   resd2=y2;
                   break;}
               div22=div2;
               div2=y2;
            }

           //BigInteger resd1,resd2,tres,ires;
           //resd1=BigInteger.valueOf(Math.sqrt(d1));
           //resd2=BigInteger.valueOf(Math.sqrt(d2));
           tres=resd1.multiply(resd2);
           int b1,b2,b3,b4,b5,b6,b7,b8;
           BigInteger a1,a2,a3,a4,a5,a6,a7,a8,mul1,mul2,mul3,mul4,add1,add2,add3;
           BigDecimal resu;
           b1=intArray[0];
           a1=BigInteger.valueOf(b1);
           b2=intArray[1];
           a2=BigInteger.valueOf(b2);
           b3=intArray[2];
           a3=BigInteger.valueOf(b3);
           b4=intArray[3];
           a4=BigInteger.valueOf(b4);
           b5=intArray[4];
           a5=BigInteger.valueOf(b5);
           b6=intArray[5];
           a6=BigInteger.valueOf(b6);
           b7=intArray[6];
           a7=BigInteger.valueOf(b7);
           b8=intArray[7];
           a8=BigInteger.valueOf(b8);
           mul1=a1.multiply(a5);
           mul2=a2.multiply(a6);
           mul3=a3.multiply(a7);
           mul4=a4.multiply(a8);
           
           add1=mul1.add(mul2);
           add2=add1.add(mul3);
           add3=add2.add(mul4);
          
           //ires=BigInteger.valueOf(intArray[0]*intArray[4]+intArray[1]*intArray[5]+intArray[2]*intArray[6]+intArray[3]*intArray[7]);   
       BigDecimal numm,denom;
       numm=new BigDecimal(add3);
       denom=new BigDecimal(tres);
       resu=numm.divide(denom,BigDecimal.ROUND_HALF_UP);
       
       
       //double res=ires/tres;
       double res=resu.doubleValue();
       double threshold=0.8;
       int exe=0;
       if(res>=threshold){
       exe=1;
       }
       else{
       exe=2;
       }
       //int vv=intArray[0];
       result.set(exe);
       //String val=Double.toString(res);
       context.write(new Text("Total execution phases"),result);
    }
  }

public static class CosineSimilarityReducer
       extends Reducer<Text,IntWritable,Text,IntWritable> {
       private IntWritable result = new IntWritable();
      
       public void reduce(Text key,IntWritable values,
                       Context context
                       ) throws IOException, InterruptedException {
                       //double threshold=0.2;
                       //StringTokenizer itr1 = new StringTokenizer(value.toString());
                       //int exe=0;
                       /*for (Text val : values) {
                       String inres=val.toString();
                       //String inres=itr1.nextToken();
                       float innres=Float.valueOf(inres);
                       if(innres<=threshold){
                       exe=1;
                       }
                       else{
                       exe=2;
                       }
                       }
                       //result.set(exe);*/
                      /* int[] intArray1= new int[10];
                       int i=0;
                       int temp=1;
						for (IntWritable val:values) {
						   intArray1[i++]=val.get();
						}
						for(i=0;i<8;i++){
						if(temp==1&&intArray1[i]==1&&intArray1[i+1]==1){
						temp=1;
						}
						else if(temp==2&&intArray1[i]==1&&intArray1[i+1]==1){
						temp=temp;
						}
						else{
						temp=temp+1;
						}
						}
                       result.set(temp);*/
                       
      				   context.write(key,values);
  }
      }
                      
      
      
     
public static void main(String[] args) throws Exception {
   Configuration conf = new Configuration();
   
   
   Job job = Job.getInstance(conf,"cosine");
   job.setJarByClass(CosineSimilarity.class);
   
   job.setMapperClass(CosineSimilarityMapper.class);
   job.setCombinerClass(CosineSimilarityReducer.class);
   job.setReducerClass(CosineSimilarityReducer.class);

      
   job.setOutputKeyClass(Text.class);
   job.setOutputValueClass(IntWritable.class);
   
      
   job.setInputFormatClass(TextInputFormat.class);
   job.setOutputFormatClass(TextOutputFormat.class);
   
   TextInputFormat.addInputPath(job,new Path(args[0]));
   TextOutputFormat.setOutputPath(job,new Path(args[1]));
   
   //job.waitForCompletion(true);
   System.exit(job.waitForCompletion(true) ? 0 : 1);
}}
                      
              
